[[ $- != *i* ]] && return

colors() {
  local fgc bgc vals seq0

  printf "Color escapes are %s\n" '\e[${value};...;${value}m'
  printf "Values 30..37 are \e[33mforeground colors\e[m\n"
  printf "Values 40..47 are \e[43mbackground colors\e[m\n"
  printf "Value  1 gives a  \e[1mbold-faced look\e[m\n\n"

  # foreground colors
  for fgc in {30..37}; do
    # background colors
    for bgc in {40..47}; do
      fgc=${fgc#37} # white
      bgc=${bgc#40} # black

      vals="${fgc:+$fgc;}${bgc}"
      vals=${vals%%;}

      seq0="${vals:+\e[${vals}m}"
      printf "  %-9s" "${seq0:-(default)}"
      printf " ${seq0}TEXT\e[m"
      printf " \e[${vals:+${vals+$vals;}}1mBOLD\e[m"
    done
    echo; echo
  done
}

[[ -f ~/.extend.bashrc ]] && . ~/.extend.bashrc

[ -r /usr/share/bash-completion/bash_completion ] && . /usr/share/bash-completion/bash_completion

archey3 &> /dev/null; if [[ $? == 0 ]]; then archey3; fi


# colors for prompt
BLK="\e[0;30m"
RED="\e[0;31m"
GRN="\e[0;32m"
YEL="\e[0;33m"
BLU="\e[1;34m"
MGT="\e[0;35m"
CYN="\e[0;36m"
GRY="\e[1;37m"
RST="\e[0m"

printGit() {
  local BRANCH=$(git rev-parse --abbrev-ref HEAD 2> /dev/null)
  local COMMIT=$(git rev-parse --short HEAD 2> /dev/null)
  echo -e "$MGT($BRANCH@$COMMIT)$RST"
}

printReturnCode() {
  if [[ $1 == 0 ]]; then
    echo -ne "${GRN}ok$RST"
  else
    echo -ne "$RED$1$RST"
  fi
}

# set prompt to:
# (blank line)
# <errno>
# <user>@<machine> <pwd> [(<branch>@<commit>)]
# <hist>: [sudo] $>
PS1="\n\$(printReturnCode \$?)\n"
PS1="$PS1\[$BLU\]\u@\h\[$RST\] \[$GRN\]\w\$(if [ '\$(pwd)' != '/' ]; then echo '/'; fi)\[$RST\] "
PS1="$PS1\$(if git rev-parse --git-dir &> /dev/null; then printGit; fi)"
PS1="$PS1\n"
PS1="$PS1\[$GRN\]\!:\[$RST\] \$(if sudo -nv 2> /dev/null; then echo -ne '$RED[sudo]$RST '; fi)"
PS1="$PS1\[$GRN\]$>\[$RST\] "
PS2="\[$GRN\]>\[$RST\] "


# aliases
alias up='cd ..'
alias ls='LC_COLLATE=C ls --color'
alias ll='ls -lph --group-directories-first'
alias lla='ls -laph --group-directories-first'
alias sudo='sudo '
alias fuck='sudo "$BASH" -c "$(history -p !!)"'
alias dd='dd status=progress'
alias shutdown='shutdown now'
alias youtube-dl-mp3='youtube-dl -x --audio-quality 0 --audio-format mp3'
alias youtube-dl-mp4='youtube-dl -f "bestvideo+bestaudio" --merge-output-format mp4'
